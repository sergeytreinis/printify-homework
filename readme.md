
## Task description

Create a tiny RESTful web service with the following business requirements:

Application must expose REST API endpoints for the following functionality:

create product (price, productType, color, size)
calculate order price (Collection of products and quiantities)  (should also save Order draft somewhere)
list all Order drafts
list all Order drafts by productType

Service must perform operation validation according to the following rules and reject if:

type + color + size already exists
Order is empty or total price is less then 10
N orders / second are received from a single country (essentially we want to limit number of orders coming from a country in a given timeframe)

Service must perform origin country resolution using a web service and store country code together with the order draft.
Because network is unreliable and services tend to fail, let's agree on default country code - "US".

Technical requirements:

You have total control over tools, as long as application is written in PHP and Laravel framework.

What gets evaluated:

Conformance to business requirements
Code quality, including testability
How easy it is to run and deploy the service (don't make us install Oracle database please 😉
Good luck and have fun!

### How to run

* Install and up docker ([docker docks](https://docs.docker.com/install/))
* Copy `.env.example` to `.env`
* Up the project with `docker-compose up -d` and wait some minutes for the first time for the images installation
* Run `docker-compose exec app php artisan key:generate` and
      `docker-compose exec app php artisan optimize`
* Then run `docker-compose exec app php artisan migrate` to create all tables in the DB
* Go to the `http://localhost:8080`

### Routes to check the task conditions realisation

#### create product
* Route: `/product/create`
* Params: type, price, color, size
* Example: `/product/create?type=t-shirt&price=5&color=black&size=xs`

#### calculate order price 
* Route: `/orders/create`
* Params: products[], quantities[]
* Example: `/orders/create?products[]=1&products[]=2&quantities[]=3&quantities[]=1`

#### list all Order drafts
* Route: `/orders/show`
* Params: not needed (to view all orders)
* Example: `/orders/show`

#### list all Order drafts by productType
* Route: `/orders/show`
* Params: type (to filter orders by product type)
* Example: `/orders/show?type=skirt`
