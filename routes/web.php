<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// routes specified by the task condition

Route::get('/product/create', 'ProductController@create');
Route::get('/orders/show', 'OrderController@show');
Route::get('/orders/create', 'OrderController@create')->middleware('country');

// additional routes
Route::get('/product/show', 'ProductController@show');
Route::get('/', function () {
    return view('welcome');
});

Route::get('/product/new', function () {
    return view('creation');
});
