<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table='order';

    protected $fillable = ['location'];

    public $location;

    public function products() {
        return $this->belongsToMany('App\Product')->withPivot('quantity');
    }

    public static function getByProductType($type) {
        $products = self::whereHas('products', function($q) use ($type) {
            $q->where('productType', '=', $type);
        })->get();

        return $products;
    }

    public static function getByCountryAndDate($country, $date) {
        return self::where('location', $country)
            ->where('created_at', '>=', $date)
            ->get();
    }
}
