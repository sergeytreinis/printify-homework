<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProductController extends Controller
{
    public function show()
    {
        $products = \App\Product::all();

        $response = [];
        foreach ($products as $product) {
            $product->orders;

            $response[] = $product;
        }

        return response()->json($response);
    }

    public function create(Request $request)
    {
        $request->validate([
            'type' => 'required|alpha_dash',
            'price' => 'required|numeric',
            'color' => 'nullable|alpha',
            'size' => 'required|alpha_num',
        ]);

        $type = $request->input('type');;
        $price = $request->input('price');;
        $color = $request->input('color');;
        $size = $request->input('size');;

        $productExist = \App\Product::getFirstByColorAndTypeAndSize($color, $type, $size);

        if ($productExist) {
            return response()->json([
                'msg' => 'Such product already exist',
            ]);
        }

        $product = new \App\Product([
            'productType' => $type,
            'price' => $price,
            'color' => $color,
            'size' => $size
        ]);

        if ($product->save()) {
            $response = [
                'msg' => 'Created!',
                'id' => $product->getKey()
            ];
        } else {
            $response = [
                'msg' => 'Bad news form database',
            ];
        }

        return response()->json($response);
    }
}
