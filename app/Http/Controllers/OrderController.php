<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function create(Request $request)
    {
        $requestedProducts = $request->input('products');
        $quantities = $request->input('quantities');

        if (empty($requestedProducts)) {
            return response()->json(['msg' => 'Products list can\'t be empty']);
        }

        if (count($requestedProducts) !== count($quantities)) {
            return response()->json(['msg' => 'Products count and quantities count must be equals']);
        }

        $price = 0;
        foreach ($requestedProducts as $key => $productId) {
            $product = \App\Product::find($productId);
            if (empty($product)) {
                return response()->json(['msg' => "No such product with id: $productId"]);
            }
            $price += $quantities[$key] * $product->getAttributeValue('price');
        }

        if ($price < 10) {
            return response()->json(['msg' => 'Products count and quantities count must be equals']);
        }

        $country = geoip_country_code_by_name($request->ip()) !== false ? geoip_country_code_by_name($request->ip()) : 'US';

        $order = new \App\Order([
            'location' => $country
        ]);

        $order->save();

        foreach ($requestedProducts as $key => $productId) {
            $order->products()->attach($productId, ['quantity' => $quantities[$key]]);
        }

        if ($order->save()) {
            $newOrder = \App\Order::find($order->getKey());
            $newOrder->products;

            $response = [
                'msg' => 'Created!',
                'order' => $newOrder,
                'price' => $price,
            ];
        } else {
            $response = [
                'msg' => 'Bad news form database',
            ];
        }

        return response()->json($response);
    }

    public function show(Request $request)
    {
        $type = $request->input('type');
        if (!empty($type)) {
            $orders = \App\Order::getByProductType($type);
        } else {
            $orders = \App\Order::all();
        }
        $response = [];
        foreach ($orders as $order) {

            $products = $order->products;
            $price = 0;
            foreach ($products as $product) {
                $price += $product['price'] * $product['pivot']['quantity'];
            }

            $response[] = [
                'location' => $order->getAttributeValue('location'),
                'products' => $products,
                'price' => $price,
            ];
        }

        return response()->json($response);
    }

}
