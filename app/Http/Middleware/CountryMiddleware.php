<?php

namespace App\Http\Middleware;

use Closure;
use DateTime;

class CountryMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $requestPerCountryLimit = 1;
        $requestTimeframeLimit = '1 second';

        // get country code of request
        $country = geoip_country_code_by_name($request->ip()) !== false ? geoip_country_code_by_name($request->ip()) : 'US';

        // get order created not longer than a second ago (or some other timeframe) with the same location code
        $date = new DateTime;
        $date->modify("-$requestTimeframeLimit");
        $formatted_date = $date->format('Y-m-d H:i:s');

        $lastOrder = \App\Order::getByCountryAndDate($country, $formatted_date);

        // if there is some order - deny request (redirect to the root)
        if (!empty($lastOrder) && count($lastOrder) >= $requestPerCountryLimit) {
            return redirect('/');
        }

        // otherwise nex call
        return $next($request);
    }
}
