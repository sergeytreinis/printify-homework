<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table='product';

    protected $fillable = ['price', 'productType', 'color', 'size'];

    public $price;

    public $productType;

    public $color;

    public $size;

    public function orders() {
        return $this->belongsToMany('App\Order')->withPivot('quantity');
    }

    public static function getFirstByColorAndTypeAndSize($color, $type, $size) {
        return self::where('color', $color)
            ->where('productType', $type)
            ->where('size', $size)
            ->first();
    }
}
